# Opsis FEA Adlink Demo


This folder hosts the Opsis-FEA Demo software for Adlink Jetson Xavier NX (Linux Ubuntu operating system).

## Download

1. Click on the `OpsisFEA_demo.tar.xz` file above.
2. Next, click the `Download` button to save the file to your local folder (Adlink Home folder ~/).

## What will be installed

- Python 3.10 ( Version 3.10.11 as of April 2023)
- Python package virtualenv and virtualenvwrapper for virtual environment setup
- Create virtual environment opsis_fea with Python 3.10, opencv-python (version 4.7.0) and cython packages to run the Opsis FEA Demo application

## Install

1. Uncompressed the `OpsisFEA_demo.tar.xz` file.
2. Open terminal, cd ~/OpsisFEA_demo directory 
3. Run `./ai_opsis_fea.sh` to start the installation process, please enter the administrator password when prompted to proceed.
4. The installation normally takes about 5-10 minutes. 
5. When the installation completed successfully, the Opsis FEA Demo application will be running with a small window displaying facial expressions detected from the webcam

## Running Opsis FEA Adlink Demo

1. Change directory cd ~/OpsisFEA_demo.
2. Run `./ai_opsis_fea.sh`

